from django.db import models

class Team(models.Model):
    name = models.CharField(max_length=100)

    def _str_(self):
        return self.name

class Player(models.Model):
    name = models.CharField(max_length=100)
    runs = models.IntegerField()
    balls_faced = models.IntegerField()
    fours = models.IntegerField()
    sixes = models.IntegerField()
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def _str_(self):
        return self.name