from django.shortcuts import render
from .models import Team, Player
from django.db import models


def scorecard(request):
    teams = Team.objects.all()

    for team in teams:
        # Calculate team score
        team_score = Player.objects.filter(team=team).aggregate(total_score=models.Sum('runs'))
        team.score = team_score['total_score']

        # Calculate number of wickets
        team_wickets = Player.objects.filter(team=team).count()
        team.wickets = team_wickets

    context = {
        'teams': teams
    }

    return render(request, 'cricket/first.html', context)