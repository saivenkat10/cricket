from django.urls import path
from . import views

urlpatterns = [
    path('sayhello', views.say_hello, name='say_hello'),
    path('rps/direct/<str:user_choice>', views.play_rps, name='play_rps'),
    path('rps/form', views.show_rps_form, name='show_rps_form'),
    path('rps/processform', views.process_rps_form, name='process_rps_form'),
    path('rps/resetwins', views.reset_win_counts, name='reset_wins')
]